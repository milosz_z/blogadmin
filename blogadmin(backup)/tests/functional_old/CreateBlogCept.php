<?php
$I = new FunctionalTester($scenario);

$I->am('user');
$I->wantTo('create a blog');

//When

$I->amOnPage('blog');
$I->see('Blogs', 'button');

//And
$I->click('create blog');

//Then
$I->amOnPage('blog/create');
$I->submitForm('.createblog');
