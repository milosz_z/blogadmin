-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: localhost    Database: blogadmin
-- ------------------------------------------------------
-- Server version	5.7.20-0ubuntu0.17.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article_category`
--

DROP TABLE IF EXISTS `article_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article_category` (
  `article_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  KEY `article_category_article_id_index` (`article_id`),
  KEY `article_category_category_id_index` (`category_id`),
  CONSTRAINT `article_category_article_id_foreign` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `article_category_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article_category`
--

LOCK TABLES `article_category` WRITE;
/*!40000 ALTER TABLE `article_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `article_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `published_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `articles_title_unique` (`title`),
  KEY `articles_published_at_index` (`published_at`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles`
--

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` VALUES (1,'Voluptate dolore est nisi ipsam quasi odio.','Beatae molestias natus voluptas aut. Qui similique in enim deleniti. Magni quam magni et dicta at dolorum odit nesciunt. Quisquam quia et beatae.','cumque',1,'2018-03-06 09:53:00','2018-03-06 09:53:00','0000-00-00 00:00:00'),(2,'Distinctio sint enim exercitationem maxime quia dolor.','Et molestias illo necessitatibus illo et. Repellat ducimus quis tempore eius hic. Quae sed iste repellat. Fuga officia est voluptate voluptatem soluta provident et. Eius distinctio velit omnis repellat.','rerum',1,'2018-03-06 09:53:00','2018-03-06 09:53:00','0000-00-00 00:00:00'),(3,'Autem placeat veritatis facilis et ea ea in.','Sint voluptatem quaerat aperiam praesentium quia assumenda. Id voluptatum culpa aut dolorem sequi est ut. Consequuntur unde tenetur ut veritatis. Eligendi cupiditate quia sunt esse inventore dolorum. Aut dolor dolor natus ut.','quaerat',1,'2018-03-06 09:53:00','2018-03-06 09:53:00','0000-00-00 00:00:00'),(4,'Commodi recusandae illo quis aut quia.','Sed et dicta officia impedit quis pariatur consequatur tempora. Quod voluptatem quia quis enim vero sed sed. Voluptatem iste maxime quia quas quia ipsa quae. Saepe reprehenderit eum ut veniam autem.','dolor',1,'2018-03-06 09:53:00','2018-03-06 09:53:00','0000-00-00 00:00:00'),(5,'Quis architecto nemo voluptas est.','Beatae non minima aspernatur. Qui porro impedit est qui. Vel quis ipsam nihil optio non voluptatum excepturi error.','consequatur',1,'2018-03-06 09:53:00','2018-03-06 09:53:00','0000-00-00 00:00:00'),(6,'Et numquam rerum tempore autem exercitationem incidunt.','Totam a voluptatibus voluptatem repudiandae blanditiis similique. Distinctio dicta pariatur consequatur rerum quo cum. Aut est officiis necessitatibus quo.','ut',1,'2018-03-06 10:02:10','2018-03-06 10:02:10','0000-00-00 00:00:00'),(7,'Modi saepe vel aut eum quia.','Qui omnis occaecati eum quia et. Sint dolor mollitia ut rerum sunt quia qui. Laborum ipsa facere velit sapiente ipsum. Corrupti nesciunt nulla mollitia veniam ut.','animi',1,'2018-03-06 10:02:10','2018-03-06 10:02:10','0000-00-00 00:00:00'),(8,'Et perspiciatis est ratione sed quo enim.','Odit voluptates omnis laudantium consequatur et molestias dolores. Earum ut voluptatem velit vitae et unde. Dignissimos officiis sint quae ut aut illum quia. Sapiente adipisci eum nulla sed et animi ab.','et',1,'2018-03-06 10:02:10','2018-03-06 10:02:10','0000-00-00 00:00:00'),(9,'Qui id culpa quasi magni.','Laudantium tempora a quia exercitationem. Veritatis distinctio numquam enim voluptate necessitatibus et tenetur dicta. Eveniet voluptatem ut nostrum debitis voluptatem. Et id quisquam corrupti et deleniti.','accusantium',1,'2018-03-06 10:02:10','2018-03-06 10:02:10','0000-00-00 00:00:00'),(10,'Ullam atque debitis nisi et sapiente.','Qui aut sint eum adipisci. Fugit distinctio et rerum totam reprehenderit eos ea. Doloribus eaque aut molestiae error qui corporis. Distinctio excepturi debitis officia error.','est',1,'2018-03-06 10:02:10','2018-03-06 10:02:10','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `detail` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_title_unique` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'HTML','html',NULL,NULL),(2,'PHP','php',NULL,NULL),(3,'CSS','css',NULL,NULL);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2016_02_19_100050_create_roles_table',1),('2016_02_19_100108_create_permissions_table',1),('2016_02_19_100123_create_articles_table',1),('2016_02_19_100137_create_categories_table',1),('2016_02_19_100200_create_role_user_table',1),('2016_02_19_100219_create_permission_role_table',1),('2016_02_19_100236_create_article_category_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`user_id`),
  KEY `role_user_user_id_foreign` (`user_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'milosz','milosz@example.com','$2y$10$WDuADgmnHJtfNzkhMVf1seA0MWX19GIMgNKTJn5Pq9hiSjMyuvB7W','pfYF1WytsFQezAIJqk9j9eJKnfoTa3trmpvvxqpaL700srTzVhcIvYLj8lYM',NULL,'2018-03-06 11:34:13'),(2,'Zachery Dietrich','thora96@example.net','$2y$10$JtBElH0a.gFnPSFT/NgLTuvijUC0oL.ScdO5TQeXLuTIxAPiCDqt6','gIxFQIAyng','2018-03-06 10:02:10','2018-03-06 10:02:10'),(3,'Dr. Wayne Bechtelar I','ubednar@example.com','$2y$10$b280/MqcOQoHrgfCbs5RNOXzczhaK/tmuK7Zpu2DcnOfkijLUQRxG','tqEM1nOthJ','2018-03-06 10:02:10','2018-03-06 10:02:10'),(4,'Christophe Haag','lockman.malika@example.net','$2y$10$pG8lrEP0m74nrCG6AfBUr.AzZTtTiM14wC3v1B8PEjGjUyZKI8mJm','4tWByXnH1j','2018-03-06 10:02:10','2018-03-06 10:02:10'),(5,'Queen Carter','stanton59@example.com','$2y$10$ML0XLNFE.Z778Uetvp47NOhn95Y719zHxoF.9pbSDe6iD1Win0n1G','G0HWSXp3TN','2018-03-06 10:02:10','2018-03-06 10:02:10'),(6,'Dr. Henriette Morar','cooper.roberts@example.org','$2y$10$JYzVbIbZsaDQZ0GTdvZdcOEk0zF0yylLneA/Lu4yvNmBzdE3hYgki','DLmYy0rltD','2018-03-06 10:02:10','2018-03-06 10:02:10'),(7,'Logan Ratke','wuckert.bria@example.com','$2y$10$aEWaXzAzOZhZwyx6H7RPIua4l8Xc5WtthjsZDYQ3WfugY6rm7UlYK','DYBtKxQ7C0','2018-03-06 10:02:10','2018-03-06 10:02:10'),(8,'Shemar Davis Sr.','wcruickshank@example.net','$2y$10$i1C1sMXq5G/sA94/j.dFreHl5PzytAPUlMfVxF3sU/fBqg5TnUbNi','696rgmrWPK','2018-03-06 10:02:10','2018-03-06 10:02:10'),(9,'Elliott Douglas','xhilll@example.net','$2y$10$h6DWoQvAsClije6.rQHOweOc2A00fVpVMhHWxHE8z332b/hBm9AFW','c48R7m1sM7','2018-03-06 10:02:10','2018-03-06 10:02:10'),(10,'Garland Hegmann PhD','predovic.conor@example.net','$2y$10$beIbbhfeiirNuCZHAxSD/.gws2f.xfFdNVsGmzYqmbWmJCLxGXToq','YU4dYfAig4','2018-03-06 10:02:10','2018-03-06 10:02:10'),(11,'Emmy Heller MD','pete.miller@example.org','$2y$10$/Bad/p9pIyxnSRGxPYHDUe5cb0AFgaYXt9gwv5Tqw5daHrZSDw21u','tNo06Bbs9G','2018-03-06 10:02:10','2018-03-06 10:02:10'),(12,'Tod Johnston','white.hermina@example.com','$2y$10$31WQ9xPMRiKFMOZDKN8mIuZCI/HrS3lf3Se/bt6TA2KZfGNOVdyLm','Lli02imcQN','2018-03-06 10:02:10','2018-03-06 10:02:10'),(13,'Leopold Hessel','destin01@example.com','$2y$10$W2QquYcQd.juz9cZiJGouejPapPMhGZqKnn/tnTPP6Tej1XKx5un.','qPLFjxXxvt','2018-03-06 10:02:10','2018-03-06 10:02:10'),(14,'Lelia Lockman','nhomenick@example.net','$2y$10$.jXZZqOq4gq/T6cPjXoM9u2BzAhEsP.ISzoeze1ucjNH4dtMcCPJC','YwDMRN3k6Q','2018-03-06 10:02:10','2018-03-06 10:02:10'),(15,'Dr. Pablo Nolan','stroman.marshall@example.com','$2y$10$PLvFWeOMlizOAiHSJdYceuqeB41zUEWvQlop3OnzmPviuALsgsPQ6','vllZq5Qu1P','2018-03-06 10:02:10','2018-03-06 10:02:10'),(16,'Rafael Corkery','lenny.bailey@example.org','$2y$10$DyO46FXoYBwKnq0ZE30SSeFi3qYLwqMbOpaJLGvad8zeAelPsiruK','9dGeEvF1Z3','2018-03-06 10:02:10','2018-03-06 10:02:10'),(17,'Paxton Olson PhD','humberto58@example.com','$2y$10$fWtY6rJIsx69i/jDNS/MnuHi9NK.5vagCOBO67U.LoEYwwLm6.iZq','1P1HqDgBxN','2018-03-06 10:02:10','2018-03-06 10:02:10'),(18,'Oleta Marquardt','joaquin23@example.net','$2y$10$MZn0MRzM6mKZmPxWykXwg.zSBsr62ujckVKdo2O6Axk4ITYrGBj9.','w6LTqok5Yy','2018-03-06 10:02:10','2018-03-06 10:02:10'),(19,'Mr. Adolph Pagac','kassandra83@example.org','$2y$10$FtSHfPtXPmxwpLvTVrJv3eAR31e/JFs8/Iw8T3xODVT/w/4x80ZSu','i4w2PiGq4Z','2018-03-06 10:02:10','2018-03-06 10:02:10'),(20,'Giovanni Barrows','gust91@example.com','$2y$10$3B3Y1zHeuHdd2DoAUoFpGe/XxZQnRmXWWeaZwfCMAv5VuPQWc6oeq','pDSFJatYvh','2018-03-06 10:02:10','2018-03-06 10:02:10'),(21,'Lottie Considine','jpaucek@example.org','$2y$10$sAQicvQGdymWWM1SifRPkuF38T6fmciF.Tkl1UWEDhw29Rc4h3wD6','3DgV2NpYzp','2018-03-06 10:02:10','2018-03-06 10:02:10'),(22,'Micaela Jenkins','hpfeffer@example.com','$2y$10$HtLT7dzqlyW5aG0R51DA/.40P7ZNVOAZ.BUwbE0lRWDQl/04ZrU7m','nxKnZrLHSv','2018-03-06 10:02:10','2018-03-06 10:02:10'),(23,'Laney Bergstrom MD','tess.medhurst@example.com','$2y$10$PGV7QNfw.n7pU3tkJQ.eSuD6QNtPT6rcrvfazkTMo0MF620HHRPwy','TwOevjIenc','2018-03-06 10:02:10','2018-03-06 10:02:10'),(24,'Reynold Anderson IV','antoinette36@example.net','$2y$10$U.lcBh8DHlMnjIfjKv.WoO8099j52NGfzfDO2T2D7Xz0oMEceilrW','lBLamzbP95','2018-03-06 10:02:10','2018-03-06 10:02:10'),(25,'Zackary Deckow','gaylord.elnora@example.net','$2y$10$RUJsn3rMIH7BOjt71UHEp.B.XQO9xi42GKEiKOJqK7lbJnbcG0d5m','i6hDWivm4S','2018-03-06 10:02:10','2018-03-06 10:02:10'),(26,'Mrs. Yasmeen Hartmann','onie31@example.com','$2y$10$aFDUD3hhKTa6G0UdnRC8tewoZnfuKxPNP2iKdr8G4O9wuoXcgfsnu','IEqDUEab0R','2018-03-06 10:02:10','2018-03-06 10:02:10'),(27,'Prof. Reggie Conn','lschowalter@example.net','$2y$10$lwvExKut5uWc09Eo.3i/F.iySsJ1GfNweWWeIng01jRUXEM5Ixkpu','5nvp1ocFAO','2018-03-06 10:02:10','2018-03-06 10:02:10'),(28,'Marquis Tremblay Sr.','oscar38@example.net','$2y$10$NiYQhiDXNI/TtzhT8aaYxOwbm3Hcuu7Xxl0JJv4bXB/FBPbQcY5G2','Tfnt9Zgak0','2018-03-06 10:02:10','2018-03-06 10:02:10'),(29,'Ms. Damaris Jacobson DDS','amelie.ferry@example.net','$2y$10$RXfPQ7vVmd0GbM7jaI2Co.rqz9cFYRCAu2PgsieKui44I/qaMylZm','053zEAz7bR','2018-03-06 10:02:10','2018-03-06 10:02:10'),(30,'Dr. Courtney Lynch','dickinson.rowland@example.org','$2y$10$sn2dNJC5yIcrTLxxvuTK8u63ZUjzCOUuYLMu8pjHtlOt2r8PCZeI2','2mbkZadwPc','2018-03-06 10:02:10','2018-03-06 10:02:10'),(31,'Nigel Bernier','etha18@example.org','$2y$10$ubvLW9IUXriurd5Pnpajp.S8YEPklEUpuWpkZbbyxC61FxzRaqIXi','aNcqdNGNnG','2018-03-06 10:02:10','2018-03-06 10:02:10'),(32,'Felipa Bogan','qhuel@example.net','$2y$10$2LcrLxcdmQNYxibfe8/IoO2hA4.PdvYuJbJtAVe6y6eLwHgt98dTK','MpZqeCJGTr','2018-03-06 10:02:10','2018-03-06 10:02:10'),(33,'Eveline Toy','ruecker.katlyn@example.org','$2y$10$1YapbwkLA2UjCh8spbFaZOHhhZMK2OOr5ZvfwSc1YMNtMW0ecaSZu','c7hdwC7xha','2018-03-06 10:02:10','2018-03-06 10:02:10'),(34,'Mr. Rogers VonRueden V','kfeest@example.net','$2y$10$y0NOoeyqglL76DDYfiAkLOTFN4.sUGwrMHEyi35a98Kpb6CKluxxK','nXbBtTUPDc','2018-03-06 10:02:10','2018-03-06 10:02:10'),(35,'Mrs. Karli Gleichner V','merlin19@example.com','$2y$10$QDhjWN6.lfi9IABro5AfEe/kTzKPtYtiCNzmlWMFJTdX48F5HlAZK','5wOh0BQA13','2018-03-06 10:02:10','2018-03-06 10:02:10'),(36,'Jane Purdy','jkreiger@example.com','$2y$10$Qej9FjOZR1jLx7528CpjIOadNrk3IphORgmX0EqVD8bOe/UB1ZgNO','f7nOMvjxBD','2018-03-06 10:02:10','2018-03-06 10:02:10'),(37,'Kameron Rempel','hoeger.alicia@example.org','$2y$10$FY3RKeHZ.0ojiaQgsrrNUOrgicDUAbMEeNGd7p2ZSWneqG.GR/c2y','xQVhFf60DT','2018-03-06 10:02:10','2018-03-06 10:02:10'),(38,'Mr. Kellen Nienow','alexa07@example.net','$2y$10$B1SqSSqGYe5tNMf4uluhR.yBJt1ao8BLqAZo/A3uljCUY7P5DUlHS','NwLKSGbQKP','2018-03-06 10:02:10','2018-03-06 10:02:10'),(39,'Alexanne Abshire','wyman.matteo@example.com','$2y$10$td.WBKFIoH4IA50OCVBHI.0v.8KRkZPqnvW59icNoFjKrWOb1YYpS','e4lTeCNQUR','2018-03-06 10:02:10','2018-03-06 10:02:10'),(40,'Daniela Cummerata','hector11@example.com','$2y$10$ge.OpsD30qtF0L6jgdZ5Juxd4aQqeIIwNyCLL8M1V7sitgNH0Igcq','ZshiMaCtas','2018-03-06 10:02:10','2018-03-06 10:02:10'),(41,'Cole Berge','kyleigh.cummerata@example.com','$2y$10$pU1JAOOFA2ua648IiK/vC.LsqyVY4Su9ZHFj0wixl46qhCSK7J.z2','c7gBeqJgFB','2018-03-06 10:02:10','2018-03-06 10:02:10'),(42,'Clay Heller V','whudson@example.net','$2y$10$DsDOiwhTyY/r1aF2O84X2Oj/AkGCsUFA1hneV.218pf5Bc0Z9GQ0O','rmvYLKKJIv','2018-03-06 10:02:10','2018-03-06 10:02:10'),(43,'Cortney Kovacek','yfritsch@example.org','$2y$10$t2i/Jr2/Jk7zhIGBkzRTEO35OUIOtYYXBInWDbh1ocqivW7qug9za','r19fM3X53w','2018-03-06 10:02:10','2018-03-06 10:02:10'),(44,'Maybell Shields','cicero.hirthe@example.com','$2y$10$O0O5KUqcnqfmINbCbG9IK.ezZ9YdfRWlA49QzSt2VXfYByrdbrkWm','FFCsiIGgR7','2018-03-06 10:02:10','2018-03-06 10:02:10'),(45,'Al Shields','sasha.barrows@example.org','$2y$10$TmUwMVSy9zD1lEDyDVBleeUt72xPlD1hUSn/DJGX2PrLuopOH6L06','mqVkjBx8Jv','2018-03-06 10:02:10','2018-03-06 10:02:10'),(46,'Mr. Hayley Runolfsson MD','parker.katlyn@example.com','$2y$10$uWZrDHC9inuel6jMu0zRpuF5mRWXuqTjoDYROxhfDSYVhkBDPbyS6','8eZRkpPrF5','2018-03-06 10:02:10','2018-03-06 10:02:10'),(47,'Keely Leffler','grant.richard@example.com','$2y$10$UVaEH1R6Nk7aeVcaX1Snc.1NhLGgPppcPzWd4w5pUaKqdHPLOIKp6','pSaEImCSKl','2018-03-06 10:02:10','2018-03-06 10:02:10'),(48,'Abner Walsh','douglas.baby@example.com','$2y$10$3xxESlTl7mJHB9lCP425we/g8gOg3vaPob3la9FQC8ERPFJz.PmA6','RcneTY5M88','2018-03-06 10:02:10','2018-03-06 10:02:10'),(49,'Dr. Kamron Heidenreich','larissa63@example.org','$2y$10$3AsCb36n555kM2bFay7UkurD6O6CRTYgg0EM11ifG8MjOe6XnKF8q','n29nEYCMx0','2018-03-06 10:02:10','2018-03-06 10:02:10'),(50,'Shayne Rutherford Sr.','walker90@example.org','$2y$10$vWSBx5bnR59L3ZwklSUgre.5DOT2anvtZkzPxLWDkQcbGgtZ8dXZS','pKI8OmOCtz','2018-03-06 10:02:10','2018-03-06 10:02:10'),(51,'Burley Beer III','harvey.zena@example.com','$2y$10$sHtHkzbGlaJTVcCQD6nSTeOveLCfK68mh..OhQMVbFBbY5GBNYjYi','s0GdR0KwCf','2018-03-06 10:02:10','2018-03-06 10:02:10');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-06 11:34:47
