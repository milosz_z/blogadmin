<?php
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('delete a category');

//When
$I->amOnPage('/admin/category');
$I->see('Category', 'h1');

//And
$I->click('Delete Category');

//Then
$I->amOnPage('/admin/category/delete');
//And
$I->see('Select Category', 'select');
$I->click('category', 'option');
//Then
$I->click('delete', 'button');

//Then
$I->seeCurrentUrlEquals('/admin/category');
$I->see('Category', 'h1');
$I->see('Category deleted!');
$I->dontSee('deleted category');
