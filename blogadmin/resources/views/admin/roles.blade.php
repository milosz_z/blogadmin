<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Roles</title>
    <!--<link rel="stylesheet" href="/css/app.css" />-->
</head>
<body>
<h1>Roles</h1>
@if (Session::has("success"))
  <p>{{ Session::get("success") }}</p>
@endif


<section>
    @if (isset ($roles))

        <ul>
            @foreach ($roles as $role)
                <li>{{ $role->name }}</li>
            @endforeach
        </ul>
    @else
        <p> no roles added yet </p>
    @endif
</section>

{{ Form::open(array('action' => 'RoleController@create', 'method' => 'get')) }}
    <div class="row">
        {!! Form::submit('Add Role', ['class' => 'button']) !!}
    </div>
{{ Form::close() }}

</body>
</html>
