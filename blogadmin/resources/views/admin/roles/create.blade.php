<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create Role</title>
    <link rel="stylesheet" href="/css/app.css" />
</head>
<body>
<h1>Add Role</h1>

{!! Form::open(array('action' => 'RoleController@store', 'class' => 'addrole')) !!}
        {{ csrf_field() }}
    <div class="row large-12 columns">
        {!! Form::label('name', 'Title:') !!}
        {!! Form::text('name', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-12 columns">
        {!! Form::label('label', 'Detail:') !!}
        {!! Form::textarea('label', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-4 columns">
        {!! Form::submit('Add Role', ['class' => 'button']) !!}
    </div>
{!! Form::close() !!}

</body>
</html>
