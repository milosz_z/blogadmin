<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middle' => ['web']], function () {
  Route::auth();

  Route::get('/home', 'HomeController@index');

  Route::resource('/admin/articles', 'ArticleController');

  Route::resource('/admin/categories', 'CategoryController' );

  Route::resource('/admin/users', 'UserController');

  Route::resource('/admin/roles', 'RoleController');
});
